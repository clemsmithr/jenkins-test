terraform {
  required_providers {
    aws = {
      version = "~> 3.24.1"
    }
    null = {
      version = "2.1"
    }
  }
}

provider "aws" {
  region = var.region
}

resource "aws_instance" "ec2" {
  count                       = 1
  ami                         = "ami-025102f49d03bec05"
  instance_type               = "t2.micro"
  subnet_id                   = "subnet-09dc04b6171db11d9"
  key_name                    = "tfkey"
  security_groups             = ["sg-0bd6506893d0dea7b"]
  associate_public_ip_address = true
  disable_api_termination     = false

  tags = {
    "Name" = "Jenkins Hook 4"
  }
}
